import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:pokedex/pokemon.dart';

class PokemonPage extends StatefulWidget {
  const PokemonPage({super.key, required this.pokedex, required this.id});

  final Pokedex pokedex;
  final int id;

  @override
  State<PokemonPage> createState() => _PokemonPage();
}

class _PokemonPage extends State<PokemonPage> {

  late Pokemon pokemon;

  bool shiny = false;

  Map<double, List<String>> resistances = {0.25:[], 0.5:[], 1:[], 2:[], 4:[]};

  @override
  void initState() {
    pokemon = widget.pokedex.pokemons[widget.id];
    if (widget.id != 0) { setResistance(); }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(pokemon.name as String),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: widget.id != 0 ? Column(
            children: [
              InkWell(
                onTap: () => setState(() {
                  shiny = !shiny;
                }),
                child: Image.network(shiny ? pokemon.sprite['shiny'] : pokemon.sprite['regular'],
                  width: 150,
                  height: 150,
                ),
              ),
              Container(
                margin: const EdgeInsets.all(20),
                child: Text('n°${pokemon.id} | ${pokemon.name}',
                  style: const TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                      fontSize: 20
                  ),
                ),
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(left: 20, right: 20),
                  child: Text('Génération : ${pokemon.generation}',
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.normal,
                        fontSize: 16
                    ),
                  )
              ),
              Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
                  child: Text('Catégorie : ${pokemon.category}',
                    style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.normal,
                        fontSize: 16
                    ),
                  )
              ),
              Container(
                  height: 30,
                  alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                  margin: const EdgeInsets.only(bottom: 10),
                  child: Row(
                    children: [
                      Text('Type${pokemon.types.length > 0 ? 's':''} : ',
                        style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                            fontSize: 16
                        ),
                      ),
                      Expanded(
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              shrinkWrap: true,
                              itemCount: pokemon.types.length,
                              itemBuilder: (context, index) {
                                return Image.network(pokemon.types[index]['image'], width: 30, height: 30,);
                              }
                          )
                      )
                    ],
                  )
              ),
              const Divider(height: 20, thickness: 2, color: Colors.red, indent: 20, endIndent: 20,),
              const Text('Résistances',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 16
                ),
              ),
              Container(
                height: 30,
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      const SizedBox(
                        width: 50,
                        child: Text('x0.25',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16
                          ),
                        ),
                      ),
                      if(resistances[0.25]!.isNotEmpty) ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: resistances[0.25]!.length,
                          itemBuilder: (context, index) {
                            return Image.network('https://raw.githubusercontent.com/Yarkis01/PokeAPI/images/types/${resistances[0.25]![index].toLowerCase()}.png', width: 30, height: 30,);
                          }
                      )
                    ],
                  ),
                )
              ),
              Container(
                height: 30,
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      const SizedBox(
                        width: 50,
                        child: Text('x0.5',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16
                          ),
                        ),
                      ),
                      if(resistances[0.5]!.isNotEmpty) ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: resistances[0.5]!.length,
                          itemBuilder: (context, index) {
                            return Image.network('https://raw.githubusercontent.com/Yarkis01/PokeAPI/images/types/${resistances[0.5]![index].toLowerCase()}.png', width: 30, height: 30,);
                          }
                      )
                    ],
                  )
                ),
              ),
              Container(
                height: 30,
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      const SizedBox(
                        width: 50,
                        child: Text('x1',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16
                          ),
                        ),
                      ),
                      if(resistances[1]!.isNotEmpty) ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: resistances[1]!.length,
                        itemBuilder: (context, index) {
                          return Image.network('https://raw.githubusercontent.com/Yarkis01/PokeAPI/images/types/${resistances[1]![index].toLowerCase()}.png', width: 30, height: 30,);
                        }
                      )
                    ],
                  ),
                )
              ),
              Container(
                height: 30,
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      const SizedBox(
                        width: 50,
                        child: Text('x2',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16
                          ),
                        ),
                      ),
                      if(resistances[2]!.isNotEmpty) ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: resistances[2]!.length,
                        itemBuilder: (context, index) {
                          return Image.network('https://raw.githubusercontent.com/Yarkis01/PokeAPI/images/types/${resistances[2]![index].toLowerCase()}.png', width: 30, height: 30,);
                        }
                      )
                    ],
                  ),
                ),
              ),
              Container(
                height: 30,
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      const SizedBox(
                        width: 50,
                        child: Text('x4',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 16
                          ),
                        ),
                      ),
                      if(resistances[4]!.isNotEmpty) ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: resistances[4]!.length,
                        itemBuilder: (context, index) {
                          return Image.network('https://raw.githubusercontent.com/Yarkis01/PokeAPI/images/types/${resistances[4]![index].toLowerCase()}.png', width: 30, height: 30,);
                        }
                      )
                    ],
                  ),
                ),
              ),
              const Divider(height: 20, thickness: 2, color: Colors.red, indent: 20, endIndent: 20,),
              const Text('Stats',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 16
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(left: 20, right: 20),
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                      children: [
                        const TextSpan(
                          text: 'HP ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        TextSpan(
                          text: pokemon.stats['hp'].toString(),
                          style: const TextStyle(
                              fontWeight: FontWeight.normal
                          ),
                        )
                      ]
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(left: 20, right: 20),
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                      children: [
                        const TextSpan(
                          text: 'Attaque ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        TextSpan(
                          text: pokemon.stats['atk'].toString(),
                          style: const TextStyle(
                              fontWeight: FontWeight.normal
                          ),
                        )
                      ]
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(left: 20, right: 20),
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                      children: [
                        const TextSpan(
                          text: 'Défense ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        TextSpan(
                          text: pokemon.stats['def'].toString(),
                          style: const TextStyle(
                              fontWeight: FontWeight.normal
                          ),
                        )
                      ]
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(left: 20, right: 20),
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                      children: [
                        const TextSpan(
                          text: 'Attaque spéciale ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        TextSpan(
                          text: pokemon.stats['spe_atk'].toString(),
                          style: const TextStyle(
                              fontWeight: FontWeight.normal
                          ),
                        )
                      ]
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(left: 20, right: 20),
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                      children: [
                        const TextSpan(
                          text: 'Défense spéciale ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        TextSpan(
                          text: pokemon.stats['spe_def'].toString(),
                          style: const TextStyle(
                              fontWeight: FontWeight.normal
                          ),
                        )
                      ]
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(left: 20, right: 20),
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                      children: [
                        const TextSpan(
                          text: 'Vitesse ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        TextSpan(
                          text: pokemon.stats['vit'].toString(),
                          style: const TextStyle(
                              fontWeight: FontWeight.normal
                          ),
                        )
                      ]
                  ),
                ),
              ),
              const Divider(height: 20, thickness: 2, color: Colors.red, indent: 20, endIndent: 20,),
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(left: 20, right: 20),
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                      children: [
                        const TextSpan(
                          text: 'Taille ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        TextSpan(
                          text: pokemon.height,
                          style: const TextStyle(
                              fontWeight: FontWeight.normal
                          ),
                        )
                      ]
                  ),
                ),
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.only(left: 20, right: 20),
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16
                      ),
                      children: [
                        const TextSpan(
                          text: 'Poids  ',
                          style: TextStyle(
                              fontWeight: FontWeight.bold
                          ),
                        ),
                        TextSpan(
                          text: pokemon.weight,
                          style: const TextStyle(
                              fontWeight: FontWeight.normal
                          ),
                        )
                      ]
                  ),
                ),
              ),
              Container(
                height: 60,
                margin: const EdgeInsets.all(20),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if(pokemon.evolution != null && pokemon.evolution['pre'] != null) ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: pokemon.evolution['pre'].length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(builder: (context) => PokemonPage(pokedex: widget.pokedex, id: pokemon.evolution['pre'][index]['pokedexId'],))
                                );
                              },
                              child: Image.network(widget.pokedex.pokemons[pokemon.evolution['pre'][index]['pokedexId']].sprite['regular'], width: 60, height: 60,),
                            );
                          }
                      ),
                      Image.network(pokemon.sprite['regular'], width: 60, height: 60,),
                      if(pokemon.evolution != null && pokemon.evolution['next'] != null) ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: pokemon.evolution['next'].length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(builder: (context) => PokemonPage(pokedex: widget.pokedex, id: pokemon.evolution['next'][index]['pokedexId'],))
                                );
                              },
                              child: Image.network(widget.pokedex.pokemons[pokemon.evolution['next'][index]['pokedexId']].sprite['regular'], width: 60, height: 60,),
                            );
                          }
                      ),
                    ],
                  ),
                ),
              )
            ],
          )
          : Image.network(pokemon.sprite['regular'],
            fit: BoxFit.fill,
            width: 500,
            height: 500,
          ),
        ),
      ),
    );
  }

  setResistance() {
    for (var resistance in pokemon.resistances) {
      if ((resistance['name'] as String).toLowerCase()   == 'électrik') {
        resistances[resistance['multiplier']]?.add('electrik');
      } else if ((resistance['name'] as String).toLowerCase()   == 'fée') {
        resistances[resistance['multiplier']]?.add('fee');
      } else if ((resistance['name'] as String).toLowerCase()   == 'ténèbres') {
        resistances[resistance['multiplier']]?.add('tenebres');
      } else {
        resistances[resistance['multiplier']]?.add(resistance['name']);
      }

    }
  }

}