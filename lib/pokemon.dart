class Pokedex {
  Pokedex({
    required this.pokemons
  });

  final List<Pokemon> pokemons;

  factory Pokedex.fromJson(List<dynamic> json) {

    List<Pokemon> pokemons = [];

    for (var pokemon in json) {
      pokemons.add(Pokemon.fromJson(pokemon));
    }

    return Pokedex(pokemons: pokemons);
  }

  int length() {
    return pokemons.length;
  }

  @override
  String toString() {

    return '''
      ${pokemons.map((e) => e.toString())}
    ''';
  }
}


class Pokemon {

  Pokemon({
    required this.id,
    required this.generation,
    required this.category,
    required this.name,
    required this.sprite,
    required this.types,
    required this.stats,
    required this.resistances,
    required this.evolution,
    required this.height,
    required this.weight
  });

  final dynamic id;
  final dynamic generation;
  final dynamic category;
  final dynamic name;
  final dynamic sprite;
  final dynamic types;
  final dynamic stats;
  final dynamic resistances;
  final dynamic evolution;
  final dynamic height;
  final dynamic weight;

  factory Pokemon.fromJson(Map<String, dynamic> json) {
    return Pokemon(
        id: json['pokedexId'],
        generation: json['generation'],
        category: json['category'],
        name: json['name']['fr'],
        sprite: json['sprites'],
        types: json['types'],
        stats: json['stats'],
        resistances: json['resistances'],
        evolution: json['evolution'],
        height: json['height'],
        weight: json['weight']
    );
  }

  @override
  String toString() {

    return '''
      id: $id
      name: $name
    ''';
  }
}