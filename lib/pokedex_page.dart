import 'package:flutter/material.dart';
import 'package:pokedex/pokemon.dart';
import 'package:pokedex/pokemon_page.dart';

class PokedexPage extends StatefulWidget {
  const PokedexPage({super.key, required this.pokedex});

  final Pokedex pokedex;

  @override
  State<PokedexPage> createState() => _PokedexPage();
}

class _PokedexPage extends State<PokedexPage> {

  final filterController = TextEditingController();

  List<Pokemon> pokemons = [];

  @override
  void initState() {
    pokemons = widget.pokedex.pokemons;

    filterController.addListener(() {
      debugPrint(filterController.text);
      filter(filterController.text);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('POKEDEX'),
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.all(10),
              child: TextFormField(
                decoration: const InputDecoration(
                  hintText: 'Recherche',
                  fillColor: Colors.white,
                  filled: true,
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.red
                    )
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.red
                      )
                  ),
                ),
                controller: filterController,
                keyboardType: TextInputType.text,
              ),
            ),
            if(pokemons.isNotEmpty) Expanded(child: 
              ListView.builder(
                shrinkWrap: true,
                itemCount: pokemons.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PokemonPage(pokedex: widget.pokedex, id: index,))
                      );
                    },
                    child: ListTile(
                      leading: Image.network(pokemons[index].sprite['regular']),
                      title: Text(pokemons[index].name),
                      subtitle: Text(pokemons[index].id.toString()),
                    ),
                  );
                }
              )
            )
          ],
        ),
      ),
    );
  }

  filter(String filter) async {
    if (filter == '') {
      setState(() {
        pokemons = widget.pokedex.pokemons;
      });
    } else {

      List<Pokemon> result = [];

      for(var pokemon in widget.pokedex.pokemons) {
        if((pokemon.name as String).toLowerCase().contains(filter.toLowerCase())) {
          result.add(pokemon);
        }
      }

      setState(() {
        pokemons = result;
      });
    }
  }

}