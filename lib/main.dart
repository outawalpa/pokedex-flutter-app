import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pokedex/pokedex_page.dart';
import 'package:pokedex/pokemon.dart';

import 'package:http/http.dart' as http;
import 'package:im_animations/im_animations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(const MyApp());

}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pokedex',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  late Pokedex pokedex;

  @override
  void initState() {

    getPokedex();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: const EdgeInsets.all(40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Rotate(
                rotationsPerMinute: 30,
                repeat: true,
                child: Image.asset('assets/images/pokeball.png'),
              ),
              const Text('LOADING',
                style: TextStyle(
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                  fontSize: 25
                ),
              )
            ],
          )
        )
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  getPokedex() async {
    final response = await http.get(Uri.parse('https://api-pokemon-fr.vercel.app/api/v1/pokemon'));

    if (response.statusCode == 200) {
      pokedex = Pokedex.fromJson(jsonDecode(response.body));
      loadPokedex();
    } else {
      throw Exception('Failed to load pokedex');
    }
  }

  loadPokedex() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => PokedexPage(pokedex: pokedex))
    );
  }
}
